<?php


namespace Ucc\Controllers;


use KHerGe\JSON\JSON;
use Ucc\Http\JsonResponseTrait;

class Controller
{
    use JsonResponseTrait;
    protected $requestBody;

    public function __construct()
    {
        $data = file_get_contents('php://input');
        $this->requestBody = json_decode($data);
    }
}
