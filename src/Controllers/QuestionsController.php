<?php


namespace Ucc\Controllers;


use Ucc\Models\Question;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        Session::set('questionsAnswered', '');

        $questions = $this->questionService->getRandomQuestions();
        $question = $questions[0];

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if (Session::get('name') === null) {
            return $this->json('You must first begin a game', 400);
        }
        $questionsAnswered = $this->questionService->getQuestionsAnswered(Session::get('questionsAnswered'));
        if (in_array($id, $questionsAnswered)) {
            $question = $this->questionService->getNextQuestion($questionsAnswered);
            return $this->json(
                [
                    'error' => 'You\'ve already answered this question.',
                    'question' => $question
                ],
                400
            );
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        $rightAnswer = $this->questionService->checkRightAnswer($id, $answer);
        if ($rightAnswer['success']) {
            Session::set('points', Session::get('points') + $rightAnswer['points']);
        }

        $answered = $this->saveQuestionsAnswered($id);

        Session::set('questionCount', intval(Session::get('questionCount')) + 1);
        $message = $rightAnswer['message'];


        $question = $this->questionService->getNextQuestion($answered);

        return $this->json(['message' => $message, 'question' => $question]);
    }

    /**
     * saveQuestionsAnswered
     *
     * @param int $id
     * @return array
     */
    private function saveQuestionsAnswered(int $id): array
    {
        $questionsAnswered = $this->questionService->getQuestionsAnswered(Session::get('questionsAnswered'));
        $questionsAnswered[] = $id;
        Session::set('questionsAnswered', implode('', $questionsAnswered));

        return $questionsAnswered;
    }
}
