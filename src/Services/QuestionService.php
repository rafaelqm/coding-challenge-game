<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    private $allQuestions;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        $this->getAllQuestions();
    }

    /**
     * getAllQuestions
     *
     * @throws \JsonMapper_Exception
     * @throws \KHerGe\JSON\Exception\DecodeException
     * @throws \KHerGe\JSON\Exception\UnknownException
     */
    public function getAllQuestions(): void
    {
        $questionsArray = $this->json->decodeFile(self::QUESTIONS_PATH);
        $this->allQuestions = [];

        foreach ($questionsArray as $question) {
            $questionObject = new Question();
            $this->allQuestions[] = $this->jsonMapper->map($question, $questionObject);
        }
    }

    public function getRandomQuestions(int $count = 5): array
    {
        if ($count > count($this->allQuestions)) {
            $count = count($this->allQuestions);
        }

        $choosedQuestions = [];
        $choosedQuestionsIds = [];
        while (count($choosedQuestions) < $count) {
            $randomQuestionNumber = rand(0, count($this->allQuestions) - 1);
            $randomQuestion = $this->allQuestions[$randomQuestionNumber];
            if (!in_array($randomQuestionNumber, $choosedQuestionsIds)) {
                $choosedQuestions[] = $randomQuestion;
                $choosedQuestionsIds[] = $randomQuestionNumber;
            }
        }

        return $choosedQuestions;
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->findQuestion($id);

        if ($question && $question->getCorrectAnswer() == $answer) {
                return intval($question->getPoints());
        }

        return 0;
    }

    public function checkRightAnswer($questionId, $answer)
    {
        $question = $this->findQuestion($questionId);

        if (!$question) {
            return [
                'success' => false,
                'message' => 'Question not found'
            ];
        }

        $points = $this->getPointsForAnswer($questionId, $answer);
        if ($points) {
            return [
                'success' => true,
                'message' => 'Right Answer',
                'points' => $points
            ];
        }

        return [
            'success' => false,
            'message' => 'Wrong answer'
        ];
    }

    private function findQuestion($id)
    {
        foreach ($this->allQuestions as $question) {
            if ($question->getId() === $id) {
                return $question;
            }
        }
        return false;
    }

    /**
     * @param array $answered
     * @return Question
     */
    public function getNextQuestion(array $answered): Question
    {
        $questions = $this->getRandomQuestions();
        $question = $questions[0];
        $position = 0;
        while (in_array($question->getId(), $answered)) {
            $position++;
            $question = $questions[$position];
        }
        return $question;
    }

    /**
     * getQuestionsAnswered
     *
     * @param string $questionsAnsweredSession
     * @return string[]
     */
    public function getQuestionsAnswered(string $questionsAnsweredSession)
    {
        $questionsAnswered = [];

        if (strlen($questionsAnsweredSession)) {
            $questionsAnswered = explode(',', $questionsAnsweredSession);
        }
        return $questionsAnswered;
    }
}
